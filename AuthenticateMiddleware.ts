import {Middleware, NextMiddlewareHandler} from "../../src/Middleware";
import {VueRouterMiddlewareData} from "../../src/VueRouterMiddlewareData";
import {Route} from "vue-router";
import {vue} from "../../src/constants/vue";

export class AuthenticateMiddleware implements Middleware<VueRouterMiddlewareData> {
    public static PREVIOUS_ROUTE: Route | undefined | null = undefined;

    process(data: VueRouterMiddlewareData, nextMiddlewareHandler: NextMiddlewareHandler<VueRouterMiddlewareData>): void {
        if (localStorage.getItem('token') === null) {
            AuthenticateMiddleware.PREVIOUS_ROUTE = data.to;
            data.vueNextHandler({name: 'login', params: {message: 'U moet inloggen om deze pagina te openen'}});
        }

        nextMiddlewareHandler(data);
    }
}

export const authenticateMiddleware = new AuthenticateMiddleware();
